package net.esperonova.plugins.movo;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

public class AliriCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length != 1) return false;

		if(sender instanceof Player) {
			Player player = (Player) sender;
			Player target = player.getServer().getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage("§cLa ludanto ne troveblis");
			}
			else if(player.getUniqueId().equals(target.getUniqueId())) {
				sender.sendMessage("§cNe eblas teleportiĝi al si mem!");
			}
			else {
				if(MovoPlugin.venigiRequests.containsKey(target.getUniqueId()) && MovoPlugin.venigiRequests.get(target.getUniqueId()).remove(player.getUniqueId())) {
					MovoPlugin.backLocations.put(player.getUniqueId(), player.getLocation());
					player.teleport(target.getLocation());
					target.sendMessage("§6§l" + player.getName() + "§r§a teleportiĝis al vi.");
					player.sendMessage("§aVi teleportiĝis al §6§l" + target.getName() + "§r§a.");
				}
				else {
					MovoPlugin.aliriRequests.computeIfAbsent(player.getUniqueId(), (_key) -> new HashSet<>()).add(target.getUniqueId());

					target.sendMessage("§6§l" + player.getName() + "§r§a volas teleportiĝi al vi. Por akcepti, tajpu §6/venigi " + player.getName() + "§r§a.");
					player.sendMessage("§aTeleportiĝ-peto estis sendita al §6§l" + target.getName() + "§r§a.");
				}
			}
		}
		else {
			sender.sendMessage("§cNur ludantoj povas uzi ĉi tiun komandon");
		}

		return true;
	}
}
