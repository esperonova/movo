package net.esperonova.plugins.movo;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

public class MovoPlugin extends JavaPlugin implements Listener {
	public static HashMap<UUID, Location> backLocations = new HashMap<>();
	public static HashMap<UUID, HashSet<UUID>> aliriRequests = new HashMap<>();
	public static HashMap<UUID, HashSet<UUID>> venigiRequests = new HashMap<>();

	@Override
	public void onEnable() {
		getCommand("aperejo").setExecutor(new AperejoCommand());
		getCommand("lito").setExecutor(new LitoCommand());
		getCommand("reen").setExecutor(new ReenCommand());
		getCommand("aliri").setExecutor(new AliriCommand());
		getCommand("venigi").setExecutor(new VenigiCommand());

		getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		backLocations.put(event.getEntity().getUniqueId(), event.getEntity().getLocation());
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		UUID uuid = event.getPlayer().getUniqueId();
		aliriRequests.remove(uuid);
		venigiRequests.remove(uuid);
		aliriRequests.forEach((key, value) -> value.remove(uuid));
		venigiRequests.forEach((key, value) -> value.remove(uuid));
	}
}
