package net.esperonova.plugins.movo;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

public class VenigiCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length != 1) return false;

		if(sender instanceof Player) {
			Player player = (Player) sender;
			Player target = player.getServer().getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage("§cLa ludanto ne troveblis");
			}
			else if(player.getUniqueId().equals(target.getUniqueId())) {
				sender.sendMessage("§cNe eblas teleportiĝi al si mem!");
			}
			else {
				if(MovoPlugin.aliriRequests.containsKey(target.getUniqueId()) && MovoPlugin.aliriRequests.get(target.getUniqueId()).remove(player.getUniqueId())) {
					MovoPlugin.backLocations.put(player.getUniqueId(), player.getLocation());
					target.teleport(player.getLocation());
					player.sendMessage("§6§l" + target.getName() + "§r§a teleportiĝis al vi.");
					target.sendMessage("§aVi teleportiĝis al §6§l" + player.getName() + "§r§a.");
				}
				else {
					MovoPlugin.venigiRequests.computeIfAbsent(player.getUniqueId(), (_key) -> new HashSet<>()).add(target.getUniqueId());

					target.sendMessage("§6§l" + player.getName() + " volas ke vi teleportiĝu al ri. Por akcepti, tajpu §6/aliri " + player.getName() + "§r§a.");
					player.sendMessage("§aTeleportiĝ-peto estis sendita al §6§l" + target.getName() + "§r§a.");
				}
			}
		}
		else {
			sender.sendMessage("§cNur ludantoj povas uzi ĉi tiun komandon");
		}

		return true;
	}
}
