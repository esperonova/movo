package net.esperonova.plugins.movo;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LitoCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length != 0) return false;

		if(sender instanceof Player) {
			Player player = (Player) sender;
			Location location = player.getBedSpawnLocation();
			if(location == null) {
				player.sendMessage("§cVi ne havas hejmliton");
			}
			else {
				MovoPlugin.backLocations.put(player.getUniqueId(), player.getLocation());
				player.teleport(location);
				player.sendMessage("§aVi teleportiĝis al la hejmlito.");
			}
		}
		else {
			sender.sendMessage("§cNur ludantoj povas uzi ĉi tiun komandon");
		}

		return true;
	}
}
